import logoImg from "../images/logo.png";


const addHeader = () => {
    const header = document.createElement('div');
    header.className = 'header';
    const image = {
        src: logoImg,
        alt: 'logo',
    };

    const menu = (wrap) => {
        const menuArr = ['Главная', 'Персонажи', 'Противники', 'Друзья'];
        const menuList = document.createElement('ul');
        menuList.className = 'header__menu-list';
        wrap.append(menuList);

        menuArr.forEach((item, index) => {
            const li = document.createElement('li');
            const a = document.createElement('a');
            li.className = 'header__menu-item';
            a.setAttribute('href', `/${item}`);
            a.textContent = item;
            li.append(a);
            menuList.append(li);
        })
        
    };
    
    const addLogo = (wrap) => {
        const logo = document.createElement('div');
        const logoImage = document.createElement('img');  
        logo.className = 'header__logo';
        wrap.prepend(logo);
        for (let key in image) {
            logoImage.setAttribute(key, image[key]);
        }
        logo.append(logoImage);
        
    };

    const wrapMenu = () => {
        const wrap = document.createElement('div');
        wrap.className = 'header__wrap-menu';
        header.append(wrap);
        menu(wrap);
        addLogo(wrap);
    };
    wrapMenu();
    return header;
};

const header = addHeader();

export {header};