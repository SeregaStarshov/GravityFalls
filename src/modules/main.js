const addMainContent = () => {
    const main = document.createElement('main');
    main.className = 'main';

    const addContainer = () => {
        const container = document.createElement('div');
        container.className = 'container';
        main.append(container);
    };
    addContainer();

    return main;
};

const main = addMainContent();

export {main};