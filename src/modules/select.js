const createSelect = (...args) => {
    const arr = args;
    const select = document.createElement('select');
    select.className = 'select-choice';
    arr.forEach((item, index) => {
        select.options[index] = new Option(item, item, true, true);
    });
    select.value = arr[0];

    return select;
};

export default createSelect;