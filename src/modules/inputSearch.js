const createInputSearch = (obj) => {
    const inputSearch = document.createElement('input');
    inputSearch.className = 'input-search';
    for (let key in obj) {
        inputSearch.setAttribute(key, obj[key]);
    }

    return inputSearch;
};

const inputSearch = createInputSearch({type: 'text', placeholder: 'Поиск'});

export {inputSearch};